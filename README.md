# Results of 'Sibyl: a Framework for Evaluating theImplementation of Routing Protocols in Fat-Trees'
This repository contains the results of the experiments described in the Sibyl paper. 

For UC1 (FRR BGP Analysis), UC2 (FRR IS-IS vs FRR Openfabric) and UC3 (RIFT-Python Analysis), There are folders containing the relative analysis results and plots for each experiments (5 RUN). 
These results includes: 
- The values of the metrics (Messaging Load, Locality, Rounds) computed for each experiments (for further details see the Sibyl paper);
- The 'radius-graph', an interactive `.html` file containing the topology labeled with the number of packets on each link; 
- The 'node-state timeline', an interactive `.html` file describing how the interactions of nodes evolves over time;
- The 'node-state graph', an interactive `.html` file describing the state changes of the nodes during the experiment.

The bgp, openfabric and isis directories contain the result of the experiment on all the test. These results contain only one run for experiment and do not include `.html` files due space limitation (the overall size of the results and the analysis is more than 100GB!).

## Plots 
Other plots containing a comparative analysis of the protocol implementations are also provided. All these are computed on 5 runs for each test. 
